__all__ = ['DebPackageGroup','DebPackageGroups']

from .packages import Package, PackageGroup, PackageGroups
import apt
import re

arch_i386 = re.compile('^i.86$')

class DebPackageGroup(PackageGroup):
    def __init__(self,name,comment=None, cpp_prefix=None, root=None, solved=False):
        """
        Parameters
        ----------
        name: the name of the object
        comment: a comment associated with the object
        cpp_prefix: prefix for cpp group names
        root: the path to a chroot used for solving dependencies
        solved: whether the dependencies are already resolved, defaults to False
        """
        super().__init__(name,comment=comment, cpp_prefix=cpp_prefix, root=root, solved=solved)

        self._deb_packages = None

    @property
    def deb_packages(self):
        if self._deb_packages is None:
            # lookup packages in cache without resolving dependencies
            self._deb_packages = []
            cache = apt.Cache(rootdir=self.root)
            with cache.actiongroup():
                for grp in [self.packages,self.extra_packages]:
                    for p in grp:
                        if grp[p].arch not in [None,'all']:
                            name = '{}:{}'.format(p,grp[p].arch)
                        else:
                            name = p
                        try:
                            d = cache[name]
                        except:
                            print ('no such package {}'.format(name))
                            continue

                        pv = grp[p].full_version
                        if pv is not None:
                            # find selected version
                            for v in d.versions:
                                if pv == v.version:
                                    break
                            else:
                                print ('could not find version {} of package {}'.format(pv,name))
                                continue
                        d.candiate = v
                        self._deb_packages.append(d)
            cache.close()
        return self._deb_packages
        
    def do_solve(self,dependencies):
        """solve all package dependencies"""

        self._deb_packages = []
        
        # get list of packages from group packages that are required for this group
        group_packages = []
        for g in dependencies:
            for p in g.deb_packages:
                group_packages.append(p.id) 
        
        remove_packages = []
        cache = apt.Cache(rootdir=self.root)
        with cache.actiongroup():
            # handle virtual packages
            remove_virtual = []
            add_virtual = []
            for p in self.packages:
                if cache.is_virtual_package(p):
                    remove_virtual.append(p)
                    for vp in cache.get_providing_packages(p):
                        pkg = DebPackage(vp.name,arch=self.packages[p].arch)
                        add_virtual.append(pkg)
            for p in remove_virtual:
                del self.packages[p]
            for p in add_virtual:
                self.packages[p.name] = p
            for p in self.packages:
                if self.packages[p].lcfg_prefix is not None and \
                   '-' in self.packages[p].lcfg_prefix:
                    # skip files marked for deletion
                    continue
                if self.packages[p].arch is not None:
                    name = '{}:{}'.format(p,self.packages[p].arch)
                else:
                    name = p
                try:
                    d = cache[name]
                except:
                    print ('no such package {}'.format(name))
                    continue
                if d.is_installed:
                    print ('package {} already installed'.format(name))
                    remove_packages.append(p)
                    continue
                pv = self.packages[p].full_version
                if pv is not None and pv != '':
                    # find selected version
                    for v in d.versions:
                        if pv == v.version:
                            break
                    else:
                        print ('could not find version {} of package {}'.format(pv,name))
                        continue
                    d.candidate = v
                try:
                    d.mark_install()
                except Exception as e:
                    print ('failed to install {}:\n{}'.format(d.name,e))

        # remove any already installed packages
        for p in remove_packages:
            del self.packages[p]
                
        for d in cache.get_changes():
            if d.id in group_packages:
                # already in one of the subgroups
                if d.name in self.packages:
                    del self.packages[d.name]
                continue
            self.deb_packages.append(d)
            vr = d.candidate.version.rsplit('-',1)
            v = vr[0]
            if len(vr)>1:
                r = vr[1]
            else:
                r = None
            n = d.name.split(':',1)[0]
            if d.candidate.architecture == 'i386':
                arch = 'i386'
            elif d.candidate.architecture == 'all':
                arch = 'all'
            else:
                arch = None
            if n in self.packages:
                self.packages[n].version = v
                self.packages[n].revision = r
                self.packages[n].arch = arch
            else:
                pkg = DebPackage(n,version=v,revision=r,arch=arch)
                if d.marked_delete:
                    pkg.lcfg_prefix='-'
                self.add_extra_package(pkg)

        cache.close()

class DebPackage(Package):
    @property
    def arch(self):
        """the architecture"""
        return self._arch
    @arch.setter
    def arch(self,a):
        if a is not None and arch_i386.match(a):
            self._arch = 'i386'
        elif a == 'noarch':
            self._arch = 'all'
        else:
            self._arch = a
    @property
    def full_version(self):
        v = None
        if self.version is not None:
            v = self.version
            if self.revision not in [None,'*']:
                v += '-' + self.revision
        return v

class DebPackageGroups(PackageGroups):
    PKG    = DebPackage
    PKGGRP = DebPackageGroup
