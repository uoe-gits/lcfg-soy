import sys
import ruamel.yaml
import networkx
from operator import itemgetter

__all__ = ['Package','PackageGroup','DependencyGroup','PackageGroups','package_from_string']

FLAG_MAP = {
    'bootonly' : 'b',
    'notboot'  : 'B',
    'reboot'   : 'r',
    'ignore'   : 'i'
}

LCFG_PREFIX = ['+','-','?',None]

class Base:
    def __init__(self,name,comment=None):
        """
        Parameters
        ----------
        name: the name of the object
        comment: a comment associated with the object
        """
        self._name = name
        self._comment = comment

    @property
    def name(self):
        """the object name"""
        return self._name
    @property
    def comment(self):
        """a comment associated with the object"""
        return self._comment

    def to_yaml(self):
        """convert the object to a yaml string"""
        if self.comment is not None:
            return '{} # {}'.format(self.name,self.comment)
        else:
            return self.name

    def to_cpp(self):
        """convert the object to a cpp string"""
        if self.comment is not None:
            return '{} /* {} */'.format(self.name,self.comment)
        else:
            return self.name

class BaseGroup(Base):
    """a dependency group """
    def __init__(self,name,comment=None, cpp_prefix=None):
        """
        Parameters
        ----------
        name: the name of the object
        comment: a comment associated with the object
        cpp_prefix: prefix for cpp group names
        """
        super().__init__(name,comment=comment)
        self._cpp_prefix = cpp_prefix

    @property
    def cpp_prefix(self):
        """the prefix for cpp group names"""
        return self._cpp_prefix

    @property
    def cpp_group_name(self):
        """the cpp group name"""
        if self._cpp_prefix is not None:
            p = self.cpp_prefix + '_'
        else:
            p = ''
        return (p+self.name.replace('-','_')).upper()

class Package(Base):
    """a package"""

    def __init__(self,name,version=None,revision=None,epoch=None,arch=None,lcfg_prefix=None,flags=None,comment=None):
        """
        Parameters
        ----------
        name: the name of the object
        comment: a comment associated with the object
        """

        super().__init__(name,comment=comment)
        self.version = version
        self.revision = revision
        self.epoch = epoch
        self.arch = arch
        self.flags = flags
        self.lcfg_prefix = lcfg_prefix

    @property
    def version(self):
        """the version of the package"""
        return self._version
    @version.setter
    def version(self,v):
        self._version =v
    @property
    def revision(self):
        """the revision of the package"""
        return self._revision
    @revision.setter
    def revision(self,r):
        self._revision = r
    @property
    def epoch(self):
        return self._epoch
    @epoch.setter
    def epoch(self,e):
        if e is None:
            self._epoch = e
        else:
            e = int(e)
            if e<0:
                raise ValueError('epoch must be a positive integer')
            self._epoch = e
    @property
    def arch(self):
        """the architecture"""
        return self._arch
    @arch.setter
    def arch(self,a):
        self._arch = a
    @property
    def full_version(self):
        v = None
        if self.version is not None:
            v = self.version
            if self.revision is not None:
                v += '-' + self.revision
        return v
    @property
    def flags(self):
        """the flags"""
        return self._flags
    @flags.setter
    def flags(self,flags):
        self._flags = ''
        if flags is not None:
            for f in flags:
                self.add_flag(f)
    def add_flag(self,flag):
        """add flag to list of flags"""
        f = None
        if flag in FLAG_MAP.values():
            f = flag
        elif flag in FLAG_MAP.keys():
            f = FLAG_MAP[flag]
        else:
            raise ValueError ('unknown flag {}'.format(flag))
        if f not in self._flags:
            self._flags += f
    @property
    def lcfg_prefix(self):
        """special package prefix used by LCFG package lists"""
        return self._lcfg_prefix
    @lcfg_prefix.setter
    def lcfg_prefix(self,l):
        if l not in LCFG_PREFIX:
            raise ValueError('unknown prefix {}'.format(l))
        self._lcfg_prefix = l

    @property
    def lcfg_package_name(self):
        """the full lcfg package name"""
        lcfg = ''
        if self.lcfg_prefix is not None:
            lcfg += self.lcfg_prefix
        lcfg += self.name
        lcfg += '='
        if self.version is None:
            lcfg += '*'
        else:
            lcfg += str(self.version)
        lcfg += '-'
        if self.revision is None:
            lcfg += '*'
        else:
            lcfg += str(self.revision)
        if self.arch is not None:
            lcfg += '/'+self.arch
        if len(self.flags) > 0:
            lcfg += ':'+self.flags
        return lcfg

    def to_yaml(self):
        """convert the object to a yaml string"""
        y = ''
        if self.lcfg_prefix is not None:
            y += self.lcfg_prefix
        y+=self.name
        if len(self.flags) > 0:
            y+=':'+self.flags
        if self.version is not None:
            y+='='+self.version
            if self.revision is not None:
                y+='-'+self.revision
        if self.arch is not None:
            y+='/'+self.arch

        if self.comment is not None:
            return '{} # {}'.format(y,self.comment)
        else:
            return y

    def to_cpp(self):
        """convert the object to a cpp string"""
        if self.comment is not None:
            return '{} /* {} */'.format(self.lcfg_package_name,self.comment)
        else:
            return self.lcfg_package_name

def package_from_string(pstring,ptype=Package):
    """create a package from string

    Paramaters
    ----------
    pstring - string describing package.

    pstring has the following format

    [LCFG_PREFIX]PKG_NAME[:FLAGS][=VERSION[-REVISION]][/ARCH]

    * LCFG_PREFIX: is one of +,-,?
    * PKG_NAME: the name of the package
    * FLAGS: a combination of b,B,r,i
    * VERSION: the version of the package
    * REVISION: the revision of the package
    * ARCH: the package architecture    
    """

    orig = pstring

    comment = None
    name = None
    version = None
    revision = None
    arch = None
    flags = None
    lcfg_prefix = None

    pstring = pstring.split('#', maxsplit=1)
    if len(pstring)>1:
        comment = pstring[1].strip()
    pstring = pstring[0].strip()

    i = 0
    if pstring[0] in LCFG_PREFIX:
        lcfg_prefix = pstring[0]
        i = 1
    pstring = pstring[i:]
    if '/' in pstring:
        pstring,arch = pstring.split('/')
    if '=' in pstring:
        pstring,vstring = pstring.split('=')
        vstring = vstring.split('-')
        if len(vstring)<1:
            raise RuntimeError('Cannot parse {}'.format(orig))
        if len(vstring)>1:
            version = '-'.join(vstring[:-1])            
            revision = vstring[-1]
        else:
            version = vstring[0]
    pstring = pstring.split(':')
    if len(pstring)>2:
        raise RuntimeError('Cannot parse {}'.format(orig))
    name = pstring[0]
    if len(pstring)>1:
        flags = pstring[1]

    pkg = ptype(name,version=version,revision=revision,arch=arch,lcfg_prefix=lcfg_prefix,flags=flags, comment=comment)
    return pkg

class DependencyGroup(BaseGroup):
    """a dependency group """
        
    def to_cpp(self):
        """convert the object to a cpp string"""
        cpp = '#define {}'.format(self.cpp_group_name)
        if self.comment is not None:
            return '{} /* {} */'.format(cpp,self.comment)
        else:
            return cpp

class PackageGroup(BaseGroup):
    def __init__(self,name,comment=None, cpp_prefix=None, root=None, solved=False):
        """
        Parameters
        ----------
        name: the name of the object
        comment: a comment associated with the object
        cpp_prefix: prefix for cpp group names
        root: the path to a chroot used for solving dependencies
        solved: whether the dependencies are already resolved, defaults to False
        """
        super().__init__(name,comment=comment, cpp_prefix=cpp_prefix)

        self._root = root
        self._packages = {}
        self._extra_packages = {}
        self._dependencies = {}
        self._solved_dependencies = solved

    @property
    def root(self):
        return self._root
        
    @property
    def packages(self):
        """a dictionary containing all the packages"""
        return self._packages
    @property
    def extra_packages(self):
        """a dictionary containing extra packages to fullfill dependencies"""
        return self._extra_packages
    @property
    def dependencies(self):
        """a dictionary containing all the depend groups"""
        return self._dependencies

    def solve_dependencies(self,dependencies):
        """solve all package dependencies"""
        if not self._solved_dependencies:
            self.do_solve(dependencies)
            self._solved_dependencies = True

    def do_solve(self,dependencies):
        """actually solve all package dependencies"""
        raise NotImplementedError
    
    def add_package(self,pkg):
        """add a package to the group

        Parameters
        ----------
        pkg: an instance of Package
        """
        assert isinstance(pkg,Package)
        if pkg.name in self.packages:
            raise RuntimeError('package {} already in list of packages'.format(pkg.name))
        self.packages[pkg.name] = pkg

    def add_extra_package(self,pkg):
        """add a package to the list of dependencies

        Parameters
        ----------
        pkg: an instance of Package
        """
        assert isinstance(pkg,Package)
        if pkg.name in self.extra_packages:
            raise RuntimeError('package {} already in list of dependencies'.format(pkg.name))
        self.extra_packages[pkg.name] = pkg
        
    def add_dependency(self,dep):
        """add a dependency group

        Parameters
        ----------
        dep: an instance of GroupDependency
        """
        assert isinstance(dep,DependencyGroup)
        if dep.name in self.dependencies:
            raise RuntimeError('dependency {} already in list of dependencies'.format(dep.name))
        self.dependencies[dep.name] = dep

    def to_yaml(self):
        """convert the object to a yaml string"""
        y = '---\n'
        if self.comment is not None:
            y += '# {}\n'.format(self.comment)
        y += 'name: {}\n'.format(self.name)
        if len(self.packages)>0:
            y += 'packages:\n'
            for p in self.packages:
                y += ' - {}\n'.format(self.packages[p].to_yaml())
        if len(self.dependencies)>0:
            y += 'dependencies:\n'
            for d in self.dependencies:
                y += ' - {}\n'.format(self.dependencies[d].to_yaml())
        y += '...\n'
        return y

    def to_cpp(self):
        """convert the object to a cpp string"""
        y = '#ifdef {}'.format(self.cpp_group_name)
        if self.comment is not None:
            y += ' /* {} */'.format(self.comment)
        y += '\n'
        for p in sorted(self.packages):
            y += '{}\n'.format(self.packages[p].to_cpp())
        for d in sorted(self.dependencies):
            y += '#ifndef {}\n'.format(self.dependencies[d].cpp_group_name)
            y += '{}\n'.format(self.dependencies[d].to_cpp())
            y += '#endif\n'
        if len(self.extra_packages)>0:
            y += '\n/* dependencies */\n'
            for p in sorted(self.extra_packages):
                y += '{}\n'.format(self.extra_packages[p].to_cpp())
        y += '#endif /* {} */\n'.format(self.cpp_group_name)
        return y

class PackageGroups:
    """a collection of package groups"""

    PKG    = Package
    PKGGRP = PackageGroup

    def __init__(self,cpp_prefix=None,root=None):
        """
        Parameters
        ----------
        cpp_prefix: prefix for cpp group names
        root: the path to a chroot used for solving dependencies
        """
        self._cpp_prefix = cpp_prefix
        self._root = root
        self._groups = {}
        self._graph = None
        self._sorted = None
        self._includes = []

    @property
    def cpp_prefix(self):
        """the prefix for cpp group names"""
        return self._cpp_prefix
    @property
    def root(self):
        """the path to a chroot for solving dependencies"""
        return self._root
    @property
    def includes(self):
        return self._includes
    @property
    def groups(self):
        """a dictionary containing all the package groups"""
        return self._groups

    @property
    def graph(self):
        """the package groups arranged as a directed graph"""
        if self._graph is None:
            self._graph = networkx.DiGraph()
            for g in self.groups:
                self._graph.add_node(g,data=self.groups[g])
            for g in self.groups:
                for d in self.groups[g].dependencies:
                    self._graph.add_edge(g,self.groups[g].dependencies[d].name)
        return self._graph

    @property
    def sorted_groups(self):
        """the list of package groups sorted by dependency starting with groups without any dependencies"""
        if self._sorted is None:
            self._sorted = []
            graph = self.graph.copy()
            while len(graph)>0:
                # loop over graph and extract leaf nodes
                nodes = []
                for n,o in sorted(graph.out_degree(), key=itemgetter(1)):
                    if o == 0:
                        nodes.append(n)
                    else:
                        break
                nodes.sort()
                # now remove leaf nodes from graph
                for n in nodes:
                    graph.remove_node(n)
                # and add them to list of sorted nodes
                self._sorted += nodes

        return self._sorted

    def selected_groups(self,prefix=None):
        """iterator over selected groups sorted reverse

        Parameters
        ----------
        prefix: filter output
        """
        for g in reversed(self.sorted_groups):
            if prefix is None or g.startswith(prefix+'_'):
                yield g
                
    def solve_dependencies(self):
        """solve all package dependencies"""
        for g in self.sorted_groups:
            dependencies = []
            for d in networkx.algorithms.traversal.depth_first_search.dfs_tree(self.graph,g).nodes():
                if d != g:
                    dependencies.append(self.groups[d])
            self.groups[g].solve_dependencies(dependencies)

    def read_yaml(self,fname):
        """read a yaml file containing package groups

        Parameters
        ----------
        fname: the name of the yaml file to be read
        """
        yaml = ruamel.yaml.YAML()

        with open(fname,'r') as stream:
            for d in yaml.load_all(stream):
                if 'name' in d:
                    pg = self.PKGGRP(d['name'],cpp_prefix=self.cpp_prefix,root=self.root)
                    if 'packages' in d:
                        for pkg in d['packages']:
                            pg.add_package(package_from_string(pkg,ptype=self.PKG))
                    if 'dependencies' in d:
                        for dep in d['dependencies']:
                            pg.add_dependency(DependencyGroup(dep,cpp_prefix=self.cpp_prefix))
                    if pg.name in self._groups:
                        print ('warning, {} already in specified'.format(pg.name))
                        continue
                    self._groups[pg.name] = pg
                else:
                    if 'includes' in d:
                        for inc in d['includes']:
                            self.includes.append(inc)
        self._graph = None
        self._sorted = None

    def read_cpp(self,fname):
        """read a cpp file containing package groups

        Parameters
        ----------
        fname: the name of the cpp file to be read
        """

        with open(fname,'r') as stream:
            pg = None
            is_dependency_group = False
            is_dependencies = False
            nr=-1
            for line in stream.readlines():
                nr = nr + 1
                line = line.strip()
                if len(line) == 0:
                    # drop empty lines
                    continue
                if line.startswith('/*') and line != '/* dependencies */':
                    # drop comments
                    continue

                if line.startswith('#ifdef'):
                    name = line.split()[1].lower()
                    is_dependencies = False
                    if pg is None:
                        pg = self.PKGGRP(name,cpp_prefix=self.cpp_prefix,root=self.root,solved=True)
                    else:
                        raise RuntimeError ('new group {} started on line {} before old group {} finished'.format(name,nr,pg.name))
                elif line.startswith('#ifndef'):
                    depname = line.split()[1].lower()
                    pg.add_dependency(DependencyGroup(depname,cpp_prefix=self.cpp_prefix))
                    is_dependency_group = True
                elif line.startswith('#define'):
                    # dependency group already dealt with
                    pass
                elif line.startswith('#endif'):
                    if is_dependency_group:
                        # dependency group is parsed
                        is_dependency_group = False
                    else:
                        # package group is parsed
                        assert pg.name not in self._groups
                        self._groups[pg.name] = pg
                        pg = None
                elif line == '/* dependencies */':
                    is_dependencies = True
                else:
                    try:
                        p = package_from_string(line,ptype=self.PKG)
                    except:
                        print ('error on line {} of {}: parsing package {}'.format(nr+1,fname,line))
                        sys.exit(1)
                    if is_dependencies:
                        try:
                            pg.add_extra_package(p)
                        except Exception as e:
                            print('warning: {}'.format(e))
                    else:
                        pg.add_package(p)


                
    def to_cpp(self,stream=sys.stdout,prefix=None):
        """print cpp output to file object
        
        Parameters
        ----------
        stream: the file object to print to
        prefix: filter output
        """
        for g in self.selected_groups(prefix=prefix):
            stream.write(self.groups[g].to_cpp())
            stream.write('\n\n')

        if len(self.includes)>0:
            stream.write('/* include extra package lists */\n')
            for inc in self.includes:
                stream.write('#include <{}>\n'.format(inc))
            stream.write('\n')

if __name__ == '__main__':
    pkgs = PackageGroups(cpp_prefix='GEOS_PKGS')
    pkgs.read_yaml(sys.argv[1])


    print (pkgs.sorted_groups)

    pkgs.to_cpp()
