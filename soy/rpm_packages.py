__all__ = ['RPMPackageGroup','RPMPackageGroups']

from .packages import Package, PackageGroup, PackageGroups

class RPMPackageGroup(PackageGroup):
    def do_solve(self,dependencies):
        """solve all package dependencies"""

        raise NotImplementedError

class RPMPackage(Package):
    pass
    
class RPMPackageGroups(PackageGroups):
    PKG    = RPMPackage
    PKGGRP = RPMPackageGroup
