# unfortunately this does not work. libsolv seems to struggle with our repos and segfaults

import pathlib
import tempfile
import shutil
import hawkey
import urllib.request, urllib.parse
from xml.dom import minidom
import configparser

def load_yum(rootdir='/'):
    sack = SoySack(rootdir=rootdir)
    yum_repos = configparser.ConfigParser()
    # loop over yum repo files
    for p in (pathlib.Path(rootdir)/'etc'/'yum.repos.d').glob('*.repo'):
        yum_repos.read(p)
    # loop over all repos
    for name in yum_repos.sections():
        priority = None
        if yum_repos.has_option(name,'priority'):
            priority = yum_repos.getint(name,'priority')
        baseurl = yum_repos.get(name,'baseurl')
        enabled = yum_repos.getboolean(name,'enabled')
        if enabled:
            sack.add_repo(name,baseurl,priority=priority)
    return sack

class SoySack:
    def __init__(self,rootdir=None):
        self._cachedir = None
        if rootdir is not None:
            self._sack = hawkey.Sack(cachedir=self.cachedir,rootdir=rootdir)
        else:
            self._sack = hawkey.Sack(cachedir=self.cachedir)
        self.sack.load_system_repo(build_cache=True)

        self._repos_enabled = {}

    @property
    def cachedir(self):
        if self._cachedir is None:
            self._cachedir = tempfile.mkdtemp(prefix='lcfg-soy-')
        return self._cachedir
    @property
    def sack(self):
        return self._sack
    @property
    def repos(self):
        return list(self._repos_enabled.keys())

    def add_repo(self,name,baseurl,priority=None, enabled = True):
        rpath = pathlib.Path(self.cachedir)/name
        if rpath.exists():
            raise RuntimeError('repo path {} already exists'.format(rpath))
        rpath.mkdir(parents=True)
        
        paths = {}
        b = baseurl.rstrip('/') + '/'
        response = urllib.request.urlopen(b+'repodata/repomd.xml')
        repomd = response.read()
        paths['repomd'] = rpath/'repomd.xml'
        with paths['repomd'].open(mode='wb') as f:
            f.write(repomd)
        repomd = minidom.parseString(repomd)
        rtypes = ['filelists','primary']
        for d in repomd.getElementsByTagName('data'):
            t = d.attributes['type'].value 
            if t in rtypes:
                r = d.getElementsByTagName('location')[0].attributes['href'].value
                response = urllib.request.urlopen(b+r)
                data = response.read()
                paths[t] = rpath/pathlib.Path(r).name
                with paths[t].open(mode='wb') as f:
                    f.write(data)

        repo = hawkey.Repo(name)
        repo.repomd_fn = str(paths['repomd'])
        repo.primary_fn = str(paths['primary'])
        repo.filelists_fn = str(paths['filelists'])
        if priority is not None:
            repo.priority = priority
        self.sack.load_repo(repo,load_filelists=True)

        if enabled:
            self.sack.enable_repo(name)
            self._repos_enabled[name] = True
        else:
            self.sack.disable_repo(name)
            self._repos_enabled[name] = False

    def enable_repo(self,name):
        if name in self._repos_enabled:
            self.sack.enable_repo(name)
            self._repos_enabled[name] = True
        else:
            raise KeyError('no such repo {}'.format(name))
    def disable_repo(self,name):
        if name in self._repos_enabled:
            self.sack.disable_repo(name)
            self._repos_enabled[name] = False
        else:
            raise KeyError('no such repo {}'.format(name))

    def cleanup(self):
        if self._cachedir is not None:
            shutil.rmtree(self.cachedir)

if __name__ == '__main__':
    if True:
        sack = load_yum('/usr/lib/yummy/sl76-mdpstable')
    else:
        sack = SoySack(rootdir='/usr/lib/yummy/sl76-mdpstable')
        #sack.add_repo('local-sl','http://mirror.is.ed.ac.uk/sites/sl/7.6/x86_64/os')
        sack.add_repo('local-epel','http://mirror.is.ed.ac.uk/sites/epel/local/x86_64/7.6')

    print (sack.cachedir)

    print (len(sack.sack))


    g = hawkey.Goal(sack.sack)

    #q = hawkey.Query(sack.sack).filter(name='python-tables')
    #for p in q:
    #    print (p.name,p.version,p.release,p.reponame)
    #g.install(p)

    q = hawkey.Query(sack.sack).filter(name='python36-aiosmtpd')
    for p in q:
        print (p.name,p.version,p.release,p.reponame)
    g.install(p)


    g.run()

    #print (g.describe_problem(0))

    print (g.list_installs())

    print ('here')
    sack.cleanup()
