import argparse
import sys
from . import PackageGroups
import jinja2
from pathlib import Path

PKG_TYPES = {}

try:
    from .deb_packages import DebPackageGroups
    PKG_TYPES['deb'] = DebPackageGroups
except:
    pass
try:
    from .rpm_packages import RPMPackageGroups
    PKG_TYPES['rpm'] = RPMPackageGroups
except:
    pass

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input',nargs='+',help='input file to be read')
    parser.add_argument('-r','--resolve',choices=PKG_TYPES.keys(),help='resolve package versions and dependencies. Must be one of {}'.format(str(list(PKG_TYPES.keys()))))
    parser.add_argument('-c','--chroot',help='location of chroot to use')
    parser.add_argument('-o','--output',type=Path,help='name of output file')
    parser.add_argument('-p','--prefix',help='only write groups with prefix PREFIX')
    parser.add_argument('-t','--template',type=Path,help="use template for writing output")
    args = parser.parse_args()

    if args.resolve is not None:
        pkgs = PKG_TYPES[args.resolve](root=args.chroot)
    else:
        pkgs = PackageGroups()
    for inname in args.input:
        if inname.endswith('.pkgs'):
            pkgs.read_cpp(inname)
        else:
            pkgs.read_yaml(inname)

    if args.output is None:
        out = sys.stdout
    else:
        out = args.output.open('w')

    if args.resolve is not None:
        pkgs.solve_dependencies()

    if args.template is not None:
        env = jinja2.Environment(
            trim_blocks=True, lstrip_blocks=True,
            loader=jinja2.FileSystemLoader(str(args.template.parent)))
        tmpl = env.get_template(args.template.name)
        out.write(tmpl.render(pkgs = pkgs,
                              groups = pkgs.selected_groups(prefix=args.prefix))
        )
    else:
        pkgs.to_cpp(stream=out,prefix=args.prefix)


if __name__ == '__main__':
    main()
