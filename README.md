# lcfg-soy
The Son of Yummy - managing rpm and apt LCFG package lists.


## Input File Specification
`lcfg-soy` reads a number of files containing groups of packages. Groups can depend on other groups. Each group of packages is represented by a YAML document. Each group must have a name and can have a list of packages and/or a list of depdencies.

Each package can be described as follows:
```[LCFG_PREFIX]PKG_NAME[:FLAGS][=VERSION[-REVISION]][/ARCH]```
where
 * `LCFG_PREFIX` can be one of +,-,?
 * `PKG_NAME` is the name of the package
 * `FLAGS` are the LCFG flags, ie can be any combination of b,B,r,i
 * `VERSION` the version of the package
 * `REVISION` the revision of the package
 * `ARCH` the architecture of the package

You can use a special yaml document to include other package lists, eg
```
---
includes:
  - some/other/package_list.pkgs
...
```

Example
 * `test`
 * `+test:br=1.2-3.4/noarch`
 * `test/noarch`

See [minimal.yaml](minimal.yaml) for an example package list.

## Installation
Check the requirements file for python dependencies. In particular the following packages are required:
 * ruamel.yaml
 * networkx

You can install the package in the usual manner, eg
```
python setup.py install
```

## Usage
`lcfg-soy` reads package specifications from a number of input yaml files. It can also read resolved pkgs (output) files. Package groups read from pkgs file will not get resovled again but can be used as dependency in the unresolved package groups read from the yaml files.

By default all package groups will be written to the output file. You can use the `-p/--prefix PREFIX` option to filter output to a specific unit. For example, the School of GeoSciences uses the prefix `geos`. GeoSciences package groups make use of the `lcfg` package groups which are handled elsewhere. Using the option `-p geos` will only write the GeoSciences package groups.

So for example
```
lcfg-soy -r deb -c /patch/to/chroot geos_*.yaml lcfg_options.pkgs -p geos -o geos_options.pkgs
```
will resolve all package groups defined in the geos_*.yaml` files using resolved package groups defined in `lcfg_options.pkgs` where required. Only groups starting with the `geos` prefix are written to the output file `geos_options.pkgs`.

## Using a chroot
Create a chroot using debootstrap, eg
```
sudo debootstrap bionic /path/to/chroot http://gb.archive.ubuntu.com/ubuntu/
```
use the -c/--chroot option to specify the location of the chroot to use. 

Remember to add your repositories to the list of repos and to run
```
apt update
```
to refresh the list of available packages.

## Using templates
The output can be passed through a jinja2 template using the `-t` option. The template should contain something like this:
```
{% for grp in groups %}
{{ pkgs.groups[grp].to_cpp() }}
{% endfor %}

{% if pkgs.includes|length > 0 %}
/* include extra package lists */
{% for inc in pkgs.includes %}
#include <{{ inc }}>
{% endfor %}
{% endif %}
```

## running the tests
```
python setup.py nosetests
```
or
```
python setup.py test
```
