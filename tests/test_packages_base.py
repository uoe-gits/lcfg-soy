from nose.tools import assert_equals, assert_is_none

from soy import packages

class TestBase:
    name = 'test'
    comment = 'a comment'

    def setup(self):
        self.a = packages.Base(self.name)
        self.b = packages.Base(self.name,comment=self.comment)

    def test_name(self):
        assert_equals (self.a.name,self.name)
        assert_equals (self.b.name,self.name)

    def test_comment(self):
        assert_is_none (self.a.comment)
        assert_equals (self.b.comment,self.comment)

    def test_to_yaml(self):
        assert_equals (self.a.to_yaml(),self.name)
        assert_equals (self.b.to_yaml(),'{} # {}'.format(self.name,self.comment))

    def test_to_cpp(self):
        assert_equals (self.a.to_cpp(),self.name)
        assert_equals (self.b.to_cpp(),'{} /* {} */'.format(self.name,self.comment))

class TestBaseGroup(TestBase):
    
    def setup(self):
        self.a = packages.BaseGroup(self.name)
        self.b = packages.BaseGroup(self.name,comment=self.comment)

    def test_cpp_prefix(self):
        assert_is_none (self.a.cpp_prefix)
        assert_is_none (self.b.cpp_prefix)

    def test_cpp_group_name(self):
        assert_equals (self.a.cpp_group_name,self.name.upper())
        assert_equals (self.b.cpp_group_name,self.name.upper())

class TestBaseGroupPrefix(TestBaseGroup):
    prefix = 'geos_pkgs'

    def setup(self):
        self.a = packages.BaseGroup(self.name,cpp_prefix=self.prefix)
        self.b = packages.BaseGroup(self.name,cpp_prefix=self.prefix,comment=self.comment)

    def test_cpp_prefix(self):
        assert_equals (self.a.cpp_prefix,self.prefix)

    def test_cpp_group_name(self):
        assert_equals (self.a.cpp_group_name,'{}_{}'.format(self.a.cpp_prefix,self.name).upper())

class TestDependencyGroup(TestBaseGroup):
    def setup(self):
        self.a = packages.DependencyGroup(self.name)
        self.b = packages.DependencyGroup(self.name,comment=self.comment)

    def test_to_cpp(self):
        cpp = '#define {}'.format(self.a.cpp_group_name)
        assert_equals (self.a.to_cpp(),cpp)
        assert_equals (self.b.to_cpp(),'{} /* {} */'.format(cpp,self.comment))

class TestDependencyGroupPrefix(TestBaseGroupPrefix):
    def setup(self):
        self.a = packages.DependencyGroup(self.name,cpp_prefix=self.prefix)
        self.b = packages.DependencyGroup(self.name,comment=self.comment,cpp_prefix=self.prefix)

    def test_to_cpp(self):
        cpp = '#define {}'.format(self.a.cpp_group_name)
        assert_equals (self.a.to_cpp(),cpp)
        assert_equals (self.b.to_cpp(),'{} /* {} */'.format(cpp,self.comment))
