from nose.tools import assert_equals, assert_is_none, assert_raises, assert_in

from soy import packages
from test_packages_base import TestBaseGroup


class TestPackageGroup(TestBaseGroup):
    def setup(self):
        self.a = packages.PackageGroup(self.name)
        self.b = packages.PackageGroup(self.name,comment=self.comment)

    def test_n_packages(self):
        assert_equals(len(self.a.packages),0)
        assert_equals(len(self.b.packages),0)

    def test_n_dependencies(self):
        assert_equals(len(self.a.dependencies),0)
        assert_equals(len(self.b.dependencies),0)

    def test_to_yaml(self):
        without_comment = '---\nname: {}\n...\n'.format(self.name)
        with_comment = '---\n# {}\nname: {}\n...\n'.format(self.comment,self.name)
        assert_equals (self.a.to_yaml(),without_comment)
        assert_equals (self.b.to_yaml(),with_comment)

    def test_to_cpp(self):
        without_comment = '#ifdef {0}\n#endif /* {0} */\n'.format(self.a.cpp_group_name)
        with_comment = '#ifdef {0} /* {1} */\n#endif /* {0} */\n'.format(self.b.cpp_group_name,self.comment)
        assert_equals (self.a.to_cpp(),without_comment)
        assert_equals (self.b.to_cpp(),with_comment)

class TestPackageGroupPackages(TestPackageGroup):
    pname = 'test_pkg'
    def setup(self):
        super().setup()

        self.package = packages.Package(self.pname)
        self.a.add_package(self.package)

    def test_n_packages(self):
        assert_equals(len(self.a.packages),1)

    def test_package(self):
        assert_in(self.pname,self.a.packages)

    def test_to_yaml(self):
        without_comment  = '---\nname: {n}\n'
        without_comment += 'packages:\n'
        without_comment += ' - {p}\n'
        without_comment += '...\n'
        without_comment = without_comment.format(n=self.name,p=self.pname)

        assert_equals (self.a.to_yaml(),without_comment)

    def test_to_cpp(self):
        without_comment = '#ifdef {0}\n{1}=*-*\n#endif /* {0} */\n'.format(self.a.cpp_group_name,self.pname)
        assert_equals (self.a.to_cpp(),without_comment)

    def test_add_package_again(self):
        assert_raises(RuntimeError,self.a.add_package,self.package)

class TestPackageGroupDependencies(TestPackageGroup):
    dname = 'test_dep'
    def setup(self):
        super().setup()

        self.dependency = packages.DependencyGroup(self.dname)
        self.a.add_dependency(self.dependency)

    def test_n_dependencies(self):
        assert_equals(len(self.a.dependencies),1)

    def test_package(self):
        assert_in(self.dname,self.a.dependencies)

    def test_to_yaml(self):
        without_comment  = '---\nname: {n}\n'
        without_comment += 'dependencies:\n'
        without_comment += ' - {d}\n'
        without_comment += '...\n'
        without_comment = without_comment.format(n=self.name,d=self.dname)

        assert_equals (self.a.to_yaml(),without_comment)

    def test_to_cpp(self):
        without_comment = """#ifdef {0}
#ifndef {1}
#define {1}
#endif
#endif /* {0} */\n""".format(self.a.cpp_group_name,self.dname.upper())
        assert_equals (self.a.to_cpp(),without_comment)

    def test_add_package_again(self):
        assert_raises(RuntimeError,self.a.add_dependency,self.dependency)

class TestPackageGroupPD(TestPackageGroup):
    pname = 'test_pkg'
    dname = 'test_dep'
    def setup(self):
        super().setup()

        self.package = packages.Package(self.pname)
        self.a.add_package(self.package)
        self.dependency = packages.DependencyGroup(self.dname)
        self.a.add_dependency(self.dependency)

    def test_n_packages(self):
        assert_equals(len(self.a.packages),1)

    def test_n_dependencies(self):
        assert_equals(len(self.a.dependencies),1)

    def test_to_yaml(self):
        without_comment  = '---\nname: {n}\n'
        without_comment += 'packages:\n'
        without_comment += ' - {p}\n'
        without_comment += 'dependencies:\n'
        without_comment += ' - {d}\n'
        without_comment += '...\n'
        without_comment = without_comment.format(n=self.name,p=self.pname,d=self.dname)

        assert_equals (self.a.to_yaml(),without_comment)

    def test_to_cpp(self):
        without_comment = """#ifdef {0}\n{1}=*-*
#ifndef {2}
#define {2}
#endif
#endif /* {0} */\n""".format(self.a.cpp_group_name,self.pname,self.dname.upper())
        assert_equals (self.a.to_cpp(),without_comment)
