from nose.tools import assert_raises, assert_equals, assert_in

from soy import packages
from test_packages_base import TestBase

rpm_packages = [
    ({'version': None, 'revision': None, 'arch': None, 'lcfg_prefix': None, 'flags': ''},'test=*-*','test'),
    ({'version': '1.2', 'revision': None, 'arch': None, 'lcfg_prefix': None, 'flags': ''},'test=1.2-*','test=1.2'),
    ({'version': None, 'revision': None, 'arch': None, 'lcfg_prefix': '+', 'flags': ''},'+test=*-*','+test'),
    ({'version': None, 'revision': None, 'arch': 'noarch', 'lcfg_prefix': None, 'flags': ''},'test=*-*/noarch','test/noarch'),
    ({'version': None, 'revision': None, 'arch': None, 'lcfg_prefix': None, 'flags': 'br'},'test=*-*:br','test:br'),
    ({'version': '1.2', 'revision': '3.4', 'arch': 'noarch', 'lcfg_prefix': '+', 'flags': 'br'},'+test=1.2-3.4/noarch:br','+test:br=1.2-3.4/noarch'),
]

class TestPackage(TestBase):
    name = 'test'

    def setup(self):
        self.a = packages.Package(self.name)
        self.b = packages.Package(self.name,comment=self.comment)

    def test_set_epoch1(self):
        e = 10
        self.a.epoch = 10
        assert_equals(self.a.epoch,e)
    def _check_set_epoch(self,e):
        self.a.epoch = e
    def test_set_epoch2(self):
        assert_raises(ValueError,self._check_set_epoch,-1)
        assert_raises(ValueError,self._check_set_epoch,'a')

    def test_set_flags(self):
        flags = 'br'
        self.a.flags = flags
        for f in flags:
            assert_in(f,self.a.flags)        
    def test_add_flag_wrong(self):
        assert_raises(ValueError,self.a.add_flag,'n')
    def test_add_flag(self):
        flags = {
            'bootonly' : 'b',
            'notboot'  : 'B',
            'reboot'   : 'r',
            'ignore'   : 'i'
        }

        for f in flags:
            self.a.add_flag(f)
            assert_in(flags[f],self.a.flags)
            self.a.flags = None
            f = flags[f]
            self.a.add_flag(f)
            assert_in(f,self.a.flags)

    def test_set_lcfg_prefix(self):
        for p in '+-?':
            self.a.lcfg_prefix = p
            assert_equals (self.a.lcfg_prefix,p)
    def _check_lcfg_prefix(self,p):
        self.a.lcfg_prefix=p
    def test_lcfg_prefix_fail(self):
        assert_raises(ValueError,self._check_lcfg_prefix,'x')

    def check_lcfg_package_name(self,spec,expected):
        for k in spec:
            setattr(self.a,k,spec[k])
        assert_equals(self.a.lcfg_package_name,expected)
    def test_lcfg_package_name(self):
        for spec,cpp,yaml in rpm_packages:
            yield self.check_lcfg_package_name,spec,cpp

    def check_yaml_name(self,spec,expected):
        for k in spec:
            setattr(self.a,k,spec[k])
        assert_equals(self.a.to_yaml(),expected)
    def test_yaml_name(self):
        for spec,cpp,yaml in rpm_packages:
            yield self.check_yaml_name,spec,yaml

    def test_to_cpp(self):
        assert_equals (self.a.to_cpp(),self.name+'=*-*')
        assert_equals (self.b.to_cpp(),'{}=*-* /* {} */'.format(self.name,self.comment))

def check_package_from_string(yaml,spec):
    pkg = packages.package_from_string(yaml)
    for k in spec:
        assert_equals (getattr(pkg,k),spec[k])
def test_package_from_string():
    for spec,cpp,yaml in rpm_packages:
        yield check_package_from_string,yaml,spec
