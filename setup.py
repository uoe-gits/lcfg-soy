#!/usr/bin/env python

from setuptools import setup

setup(name='lcfg-soy',
      setup_requires=['nose>=1.0'],
      install_requires = ['ruamel.yaml',
                          'networkx',],
      version='0.1',
      description='managing LCFG package lists',
      author='Magnus Hagdorn',
      author_email='magnus.hagdorn@ed.ac.uk',
      url='https://git.ecdf.ed.ac.uk/uoe-gits/lcfg-soy',
      packages=['soy'],
      test_suite = 'nose.collector',
      entry_points={
          'console_scripts': [
              'lcfg-soy = soy.app:main',
              ],
      },

  )
